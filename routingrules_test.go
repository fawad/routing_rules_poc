package routingrules

import (
	"fmt"
	"log"
	"os"
	"testing"
)

var rules *RoutingRules

func TestMain(m *testing.M) {
	var err error
	rules, err = New(":memory:")
	if err != nil {
		log.Fatal(err)
	}
	defer rules.Close()
	initRules(rules)
	os.Exit(m.Run())
}

func initRules(rules *RoutingRules) {
	for i := 0; i < 10000; i++ {
		rules.AddRule("bob", fmt.Sprintf("namespace-%v", i), fmt.Sprintf("pipeline-%v", i), "s3:this-should-not-be-hit")
	}
	rules.AddRule("bob", "", "", "splunk:bobs_index")
	rules.AddRule("ted", "specialnamespace", "", "splunk:teds_special_data")
	rules.AddRule("ted", "specialnamespace", "specialpipeline", "splunk:teds_very_special_data")
	rules.AddRule("ted", "internal-.*", "", "loki:teds_loki_data")
	rules.AddRule("ted", "", "", "s3:teds_s3_bucket")

}

func TestBlanketRule(t *testing.T) {
	target, err := rules.GetRule("bob", "testnamespace", "pipeline1")
	if err != nil {
		t.Errorf("Error %v", err)
	}
	if *target != "splunk:bobs_index" {
		t.Errorf("Routing to unexpected index: %v", *target)
	}
}

func TestUnknownConfig(t *testing.T) {
	target, err := rules.GetRule("thisdoesnothavearule", "randomnamespace", "specialpipeline")
	if err != nil {
		t.Errorf("Error %v", err)
	}
	if target != nil {
		t.Errorf("Routing to unexpected index: %v", *target)
	}
}

func TestSpecificRule(t *testing.T) {
	target, err := rules.GetRule("ted", "specialnamespace", "pipeline1")
	if err != nil {
		t.Errorf("Error %v", err)
	}
	if *target != "splunk:teds_special_data" {
		t.Errorf("Routing to unexpected index: %v", *target)
	}
}
func TestMoreSpecific(t *testing.T) {
	target, err := rules.GetRule("ted", "specialnamespace", "specialpipeline")
	if err != nil {
		t.Errorf("Error %v", err)
	}
	if *target != "splunk:teds_very_special_data" {
		t.Errorf("Routing to unexpected index: %v", *target)
	}
}
func TestFallbacktoGeneral(t *testing.T) {
	target, err := rules.GetRule("ted", "randomnamespace", "specialpipeline")
	if err != nil {
		t.Errorf("Error %v", err)
	}
	if *target != "s3:teds_s3_bucket" {
		t.Errorf("Routing to unexpected index: %v", *target)
	}
}
func TestRegexMatch(t *testing.T) {
	target, err := rules.GetRule("ted", "internal-spark", "pipelinenameisirrelevent")
	if err != nil {
		t.Errorf("Error %v", err)
	}
	if *target != "loki:teds_loki_data" {
		t.Errorf("Routing to unexpected index: %v", *target)
	}
}

func BenchmarkSuccessfulLookups(b *testing.B) {
	for i := 0; i < b.N; i++ {
		target, err := rules.GetRule("ted", "randomnamespace", "specialpipeline")
		if err != nil {
			b.Errorf("Error %v", err)
		}
		if *target != "s3:teds_s3_bucket" {
			b.Errorf("Routing to unexpected index: %v", *target)
		}
	}
}

func BenchmarkUnsuccessfulLookups(b *testing.B) {
	for i := 0; i < b.N; i++ {
		target, err := rules.GetRule("مرغی", "randomnamespace", "specialpipeline")
		if err != nil {
			b.Errorf("Error %v", err)
			continue
		}
		if target != nil {
			b.Errorf("Routing to unexpected index: %v", *target)
		}
	}
}
