package routingrules

import (
	"database/sql"
	"log"
	"regexp"

	sqlite "github.com/mattn/go-sqlite3"
)

// RoutingRules specifies rules for routing requests by some parameters
type RoutingRules struct {
	db         *sql.DB
	lookupStmt *sql.Stmt
}

func matches(re string, target string) bool {
	result, err := regexp.Match(re, []byte(target))
	if err != nil {
		log.Printf("Error performing match on %v, %v: %v\n", re, target, err)
		result = false
	}
	//log.Printf("matches(%v, %v) = %v\n", re, target, result)
	return result
}
func registerExtensions() {
	sql.Register("mysqlite3", &sqlite.SQLiteDriver{
		ConnectHook: func(conn *sqlite.SQLiteConn) error {
			if err := conn.RegisterFunc("matches", matches, true); err != nil {
				return err
			}
			return nil
		},
	})
}

// New creates new RoutingRules object
func New(dbPath string) (*RoutingRules, error) {
	registerExtensions()
	db, err := sql.Open("mysqlite3", dbPath)
	if err != nil {
		return nil, err
	}
	sqlStmt := `
--
CREATE TABLE IF NOT EXISTS rules (realm VARCHAR(255),
    namespace VARCHAR(255),
    pipeline VARCHAR(255),
    target_id VARCHAR(255),
    priority INTEGER DEFAULT 1
);
CREATE INDEX rules_realm ON rules(realm);
CREATE INDEX rules_namespace ON rules(namespace);
CREATE INDEX rules_pipeline ON rules(pipeline);
	`
	_, err = db.Exec(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return nil, err
	}
	stmt, err := db.Prepare("SELECT target_id FROM rules WHERE (realm IS NULL OR realm = ? OR matches(realm, ?)) AND (namespace IS NULL OR namespace = ? OR matches(namespace, ?)) AND (pipeline IS NULL OR pipeline = ? OR matches(pipeline, ?)) ORDER BY realm DESC, namespace DESC, pipeline DESC")
	if err != nil {
		return nil, err
	}
	return &RoutingRules{db, stmt}, nil
}

// Close all resources associated with routing rules
func (rules *RoutingRules) Close() error {
	rules.lookupStmt.Close()
	return rules.db.Close()
}

// AddRule adds a rule to the config database
func (rules *RoutingRules) AddRule(realm string, namespace string, pipeline string, targetID string) {
	sqlStmt := `INSERT INTO rules (realm, namespace, pipeline, target_id) VALUES (?, ?, ?, ?)`
	stmt, err := rules.db.Prepare(sqlStmt)
	if err != nil {
		log.Printf("%q: %s\n", err, sqlStmt)
		return
	}
	stmt.Exec(sql.NullString{String: realm, Valid: realm != ""}, sql.NullString{String: namespace, Valid: namespace != ""}, sql.NullString{String: pipeline, Valid: pipeline != ""}, targetID)
}

// GetRules returns
func (rules *RoutingRules) GetRule(realm string, namespace string, pipeline string) (*string, error) {
	var target *string
	rows, err := rules.lookupStmt.Query(realm, realm, namespace, namespace, pipeline, pipeline)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	idx := 0
	for rows.Next() {
		var curTarget string
		err = rows.Scan(&curTarget)
		// log.Println("Considering target for ", realm, ",", namespace, ",", pipeline, "is", curTarget)
		if err != nil {
			return nil, err
		}
		if idx == 0 {
			target = &curTarget
			break
		}
		idx++
	}

	return target, nil
}
